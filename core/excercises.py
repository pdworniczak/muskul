from properties import PUSHUPS


class Exercise():
    def __init__(self, name):
        self.name = name
        self.intervals = []

    def get_max_score(self):
        max = 0
        for interval in self.intervals:
            max = interval.max if interval.max > max else max
        return max

    def get_interval(self, result):
        for interval in self.intervals:
            if result >= interval.min and result <= interval.max:
                return interval
        if result > self.get_max_score():
            return self.intervals[-1]
        raise ValueError('no interval for score %s' % result)

    def get_scope(self, profile):
        interval = self.get_interval(profile.score.best)
        return interval.scope()

    def get_training(self, profile):
        score = profile.score

        if score.is_test():
            return None

        interval = self.get_interval(profile.score.best)
        last_serie = interval.get_day(score.day).last_serie()

        if score.serie == 0:
            # if serie == 0 that mean traning is starting we can return this day as a training
            None
        elif score.serie < last_serie[0] and score.result < last_serie[1]:
            # last trening was a failur, we return to first day of a scope
            score.day = 1
        elif score.day < interval.last_day().no:
            # until scope have days we move to the next
            score.day += 1
        else:
            score.reset()
            return None

        day = interval.get_day(score.day)
        day.get_series(profile.score.serie)
        return day

    def schedule_view(self):
        schedule = ""

        for interval in self.intervals:
            schedule += ('%s-%s\n' % interval.scope())
            for day in interval.days:
                schedule += ('\t%s: %s\n' % (day.no, day.series))

        return schedule

    def __repr__(self):
        return "%s %s" % (self.name, self.intervals)


class Interval():
    def __init__(self, min, max):
        self.min = min
        self.max = max
        self.days = []

    def get_day(self, day_no):
        for day in self.days:
            if day.no == day_no:
                return day
        raise ValueError('wrong day %s max is %s' % (day_no, self.last_day()))

    def scope(self):
        return (self.min, self.max)

    def last_day(self):
        return self.days[-1]

    def __repr__(self):
        return "%s-%s %s" % (self.min, self.max, self.days)


class Day():
    def __init__(self, no, series):
        self.no = no
        self.series = series

    def get_series(self, serie=0):
        if serie == 0 or serie in list(self.series):
            return self.series
        else:
            raise ValueError('no serie %s in %s' % (serie, list(self.series)))

    def last_serie(self):
        last_key = list(self.series)[-1]
        return (last_key, self.series[last_key])

    def __repr__(self):
        return "%s %s" % (self.no, self.series)

    def __eq__(self, other):
        if isinstance(other, Day):
            return self.no == other.no and self.series == other.series
        raise AssertionError('Can\'t compare %s and %s' % (self, other))


exercises = dict()

exercise = Exercise(PUSHUPS)
interval = Interval(0, 5)
interval.days.append(Day(1, {1: 2, 2: 3, 3: 2, 4: 2, 5: 3}))
interval.days.append(Day(2, {1: 3, 2: 4, 3: 2, 4: 3, 5: 4}))
interval.days.append(Day(3, {1: 4, 2: 5, 3: 4, 4: 4, 5: 5}))
interval.days.append(Day(4, {1: 5, 2: 6, 3: 4, 4: 4, 5: 6}))
interval.days.append(Day(5, {1: 5, 2: 6, 3: 4, 4: 4, 5: 7}))
interval.days.append(Day(6, {1: 5, 2: 7, 3: 5, 4: 5, 5: 7}))
exercise.intervals.append(interval)

interval = Interval(6, 10)
interval.days.append(Day(1, {1: 5, 2: 6, 3: 4, 4: 4, 5: 5}))
interval.days.append(Day(2, {1: 6, 2: 7, 3: 6, 4: 6, 5: 7}))
interval.days.append(Day(3, {1: 8, 2: 10, 3: 7, 4: 7, 5: 10}))
interval.days.append(Day(4, {1: 9, 2: 11, 3: 8, 4: 8, 5: 11}))
interval.days.append(Day(5, {1: 10, 2: 12, 3: 9, 4: 9, 5: 13}))
interval.days.append(Day(6, {1: 12, 2: 13, 3: 10, 4: 10, 5: 15}))
exercise.intervals.append(interval)

interval = Interval(11, 20)
interval.days.append(Day(1, {1: 8, 2: 9, 3: 7, 4: 7, 5: 8}))
interval.days.append(Day(2, {1: 9, 2: 10, 3: 8, 4: 8, 5: 10}))
interval.days.append(Day(3, {1: 11, 2: 13, 3: 9, 4: 9, 5: 11}))
interval.days.append(Day(4, {1: 12, 2: 14, 3: 10, 4: 10, 5: 15}))
interval.days.append(Day(5, {1: 13, 2: 15, 3: 11, 4: 11, 5: 17}))
interval.days.append(Day(6, {1: 14, 2: 16, 3: 13, 4: 13, 5: 19}))
exercise.intervals.append(interval)

interval = Interval(21, 25)
interval.days.append(Day(1, {1: 12, 2: 17, 3: 13, 4: 13, 5: 17}))
interval.days.append(Day(2, {1: 14, 2: 19, 3: 14, 4: 14, 5: 19}))
interval.days.append(Day(3, {1: 16, 2: 21, 3: 15, 4: 15, 5: 21}))
interval.days.append(Day(4, {1: 18, 2: 22, 3: 16, 4: 16, 5: 21}))
interval.days.append(Day(5, {1: 20, 2: 25, 3: 20, 4: 20, 5: 23}))
interval.days.append(Day(6, {1: 23, 2: 28, 3: 22, 4: 22, 5: 25}))
exercise.intervals.append(interval)

interval = Interval(26, 30)
interval.days.append(Day(1, {1: 14, 2: 18, 3: 14, 4: 14, 5: 20}))
interval.days.append(Day(2, {1: 20, 2: 25, 3: 15, 4: 15, 5: 23}))
interval.days.append(Day(3, {1: 20, 2: 27, 3: 18, 4: 18, 5: 25}))
interval.days.append(Day(4, {1: 21, 2: 25, 3: 21, 4: 21, 5: 27}))
interval.days.append(Day(5, {1: 25, 2: 29, 3: 25, 4: 25, 5: 30}))
interval.days.append(Day(6, {1: 29, 2: 33, 3: 29, 4: 29, 5: 33}))
exercise.intervals.append(interval)

interval = Interval(31, 35)
interval.days.append(Day(1, {1: 17, 2: 19, 3: 15, 4: 15, 5: 20}))
interval.days.append(Day(2, {1: 10, 2: 10, 3: 13, 4: 13, 5: 10, 6: 10, 7: 9, 8: 25}))
interval.days.append(Day(3, {1: 13, 2: 13, 3: 15, 4: 15, 5: 12, 6: 12, 7: 10, 8: 30}))
exercise.intervals.append(interval)

interval = Interval(36, 40)
interval.days.append(Day(1, {1: 22, 2: 24, 3: 20, 4: 20, 5: 25}))
interval.days.append(Day(2, {1: 15, 2: 15, 3: 18, 4: 18, 5: 15, 6: 15, 7: 14, 8: 30}))
interval.days.append(Day(3, {1: 18, 2: 18, 3: 20, 4: 20, 5: 17, 6: 17, 7: 15, 8: 35}))
exercise.intervals.append(interval)

interval = Interval(41, 45)
interval.days.append(Day(1, {1: 27, 2: 29, 3: 25, 4: 25, 5: 35}))
interval.days.append(Day(2, {1: 19, 2: 19, 3: 22, 4: 22, 5: 18, 6: 18, 7: 22, 8: 35}))
interval.days.append(Day(3, {1: 20, 2: 20, 3: 24, 4: 24, 5: 20, 6: 20, 7: 22, 8: 40}))
exercise.intervals.append(interval)

interval = Interval(46, 50)
interval.days.append(Day(1, {1: 30, 2: 34, 3: 30, 4: 30, 5: 40}))
interval.days.append(Day(2, {1: 19, 2: 19, 3: 23, 4: 23, 5: 19, 6: 19, 7: 22, 8: 37}))
interval.days.append(Day(3, {1: 20, 2: 20, 3: 27, 4: 27, 5: 21, 6: 21, 7: 21, 8: 44}))
exercise.intervals.append(interval)

interval = Interval(51, 55)
interval.days.append(Day(1, {1: 30, 2: 39, 3: 35, 4: 35, 5: 42}))
interval.days.append(Day(2, {1: 20, 2: 20, 3: 23, 4: 23, 5: 20, 6: 20, 7: 18, 8: 18, 9: 53}))
interval.days.append(Day(3, {1: 22, 2: 22, 3: 30, 4: 30, 5: 25, 6: 25, 7: 18, 8: 18, 9: 55}))
exercise.intervals.append(interval)

interval = Interval(56, 60)
interval.days.append(Day(1, {1: 30, 2: 44, 3: 40, 4: 40, 5: 55}))
interval.days.append(Day(2, {1: 22, 2: 22, 3: 27, 4: 27, 5: 24, 6: 23, 7: 18, 8: 18, 9: 58}))
interval.days.append(Day(3, {1: 26, 2: 26, 3: 33, 4: 33, 5: 26, 6: 26, 7: 22, 8: 22, 9: 60}))
exercise.intervals.append(interval)

interval = Interval(60, 100)
interval.days.append(Day(1, {1: 30, 2: 44, 3: 40, 4: 40, 5: 55}))
interval.days.append(Day(2, {1: 22, 2: 22, 3: 27, 4: 27, 5: 24, 6: 23, 7: 18, 8: 18, 9: 58}))
interval.days.append(Day(3, {1: 26, 2: 26, 3: 33, 4: 33, 5: 26, 6: 26, 7: 22, 8: 22, 9: 60}))
exercise.intervals.append(interval)

exercises[PUSHUPS] = exercise

if __name__ == '__main__':
    # print(excercises.get('pushups'))
    # print(excercises.get('pushups').intervals)
    # print(excercises.get('pushups').intervals[0])
    # print(excercises.get('pushups').intervals[0].days)
    # print(excercises.get('pushups').intervals[1].days)
    # print(excercises.get('pushups').intervals[1].scope())

    print(schedule_view(PUSHUPS))
