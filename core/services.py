from core.database import ProfileDAO
from core.model import Profile


class Profiles():

    def __init__(self):
        self.profileDAO = ProfileDAO()

    def get_profiles(self):
        profiles = self.profileDAO.get_profiles()
        return tuple(profiles)

    def get_profiles_names(self):
        return tuple([profile.name for profile in self.get_profiles()])

    def add_profile(self, profile):
        if isinstance(profile, Profile):
            self.profileDAO.add_profile((profile.name,))
        else:
            raise AttributeError()

    def del_profile(self, profile):
        self.profileDAO.del_profile((profile.name,))