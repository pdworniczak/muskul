import sqlite3
import shutil
import os
import datetime

from core.model import Profile

from logger import logging


INITIAL_DB_SENTENCES = "CREATE TABLE IF NOT EXISTS profile(id INTEGER PRIMARY KEY, name TEXT NOT NULL, email TEXT, password TEXT);"\
    "CREATE TABLE IF NOT EXISTS training_type(id INTEGER PRIMARY KEY, type TEXT);"\
    "CREATE TABLE IF NOT EXISTS training(id INTEGER PRIMARY KEY, profile_id INTEGER  NOT NULL, training_type_id INTEGER  NOT NULL, " \
                       "date DATE NOT NULL, FOREIGN KEY(profile_id) REFERENCES profile(id), " \
                       "FOREIGN KEY(training_type_id) REFERENCES training_type(id));"\
    "CREATE TABLE IF NOT EXISTS score(id INTEGER PRIMARY KEY, training_id INTEGER NOT NULL, best INTEGER NOT NULL, " \
                       "day INTEGER NOT NULL, serie INTEGER NOT NULL, result INTEGER NOT NULL, FOREIGN KEY(training_id) REFERENCES training(id));"\
    "INSERT INTO training_type (type) SELECT \'PUSHUPS\' EXCEPT SELECT type FROM training_type WHERE type = \'PUSHUPS\';"


def connect():
    if Database.connection == None:
        Database.connection = sqlite3.connect('muskul.db')
        logging.info(Database, 'connect', 'connect to muskul.db')

class Database():

    connection = None

    def __init__(self):
        connect()

    def __del__(self):
        Database.connection.close()

    def get_cursor(self):
        self.connect()
        return Database.connection.cursor()

    def disconnect(self):
        logging.info(self, 'disconnect', '')
        Database.connection.close()

    def initialize(self):
        logging.info(self, 'initialize', 'initialize database')
        Database.connection.cursor().executescript(INITIAL_DB_SENTENCES)
        Database.connection.commit()

    def get_column_names(self, cursor):
        col_names = []
        for col in cursor.description():
            col_names.append(col[0])
        return tuple(col_names)

    def backup(self):
        if os.path.isfile('muskul.db'):
            if not os.path.exists('backup'):
                os.mkdir('backup')

            filename = 'backup/muskul.db.%s' % datetime.date.today()

            if not os.path.isfile(filename):
                shutil.copyfile('muskul.db', filename)
                logging.info(self, 'backup', 'create backup %s' % filename)

class ProfileDAO:

    def add_profile(self, name):
        Database.connection.cursor().execute("INSERT INTO profile (name) values (?)", name)
        Database.connection.commit()

    def del_profile(self, name):
        Database.connection.cursor().execute("DELETE FROM profile WHERE (name) = ?", name)
        Database.connection.commit()

    def get_profiles(self):
        logging.info(self, 'get_profiles')
        cursor = Database.connection.cursor()
        cursor.execute("SELECT * FROM profile")
        results = []
        row = cursor.fetchone()
        while row:
            results.append(Profile(row[1]))
            row = cursor.fetchone()
        return results