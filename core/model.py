import datetime

__author__ = 'Paweł Dworniczak'


class Profile():

    def __init__(self, name, score=None):
        self.name = name
        self.register_date = datetime.datetime.today()
        self.password = 'test'
        self.email = 'a@b.c'
        self.score = score if score else Score()

    def __repr__(self):
        return 'Profile:\nname: %s\nreg date: %s\ne-mail: %s\nbest: %s\nday: %s\nserie: %s\nvalue: %s'\
               % (self.name, self.register_date, self.email, self.score.best , self.score.day , self.score.serie ,
                  self.score.result)


class Score():

    def __init__(self, best=0, day=0, serie=0, result=0):
        self.best = best
        self.day = day
        self.serie = serie
        self.result = result

    def set_score(self, serie, result):
        self.serie = serie
        self.result = result

    def set_best(self, best):
        self.best, self.day, self.serie, self.result = (best, 1, 0, 0)

    def reset(self):
        self.best, self.day, self.serie, self.result = (0, 0, 0, 0)

    def restart(self):
        if self.best == 0:
            self.day, self.serie, self.result = (0, 0, 0)
        else:
            self.serie, self.result = (0, 0)

    def is_test(self):
        return self.best == 0 and self.day == 0 and self.serie == 0 and self.result == 0

    def __repr__(self):
        return str((self.best, self.day, self.serie, self.result))