class labels():
    TITLE = 'Muskul'
    PROFILES = 'Profiles'
    LOGIN = 'Login'
    PUSHUPS = 'Pushups'
    INFO = 'Information'
    SCHEDULE = 'Schedule'
    ADD_TRAINING = 'Add training'
    CALENDAR = 'Calendar'


class button():
    CREATE = 'Create'
    DELETE = 'Delete'
    EXIT = 'Exit'
    OK = 'Ok'
    CANCEL = 'Cancel'
    SELECT = 'Select'
    BACK = 'Back'


class title():
    EXIT = 'Exit'
    INFO = 'Info'


class message():
    EXIT_MESSAGE = 'Do you want to exit?'
    ENTER_PROFILE_NAME = 'Enter your profile name'
    DELETE_PROFILE = 'Do you want to delete profiel %s?'
    PROFILE_EXIST = 'Profile %s allredy exist.'


PROFILES = 'profiles'
EXERCISES_TYPE = 'exercises_type'
SCHEDULE = 'schedule'
ADD_TRAINING = 'add_training'
PUSHUPS = 'pushups'
EXERCISES_TYPES = [PUSHUPS]
