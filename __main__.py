from core.database import Database
from ui import ui

database = Database()
database.initialize()
database.backup()

app = ui.Muskul()
app.mainloop()