import logging


logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)

def info(clazz, method_name, msg=None):
    logging.info('class: %s method: %s %s' % (type(clazz).__name__, method_name, ' msg: ' + msg if msg else ''))
