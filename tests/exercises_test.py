import unittest
from core.model import Profile, Score

from core.excercises import exercises
from core.excercises import Day
from core.excercises import PUSHUPS
from core.model import Score


class ExcersisesUTests(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.exercise = exercises[PUSHUPS]
        self.bad_best = Profile('')
        self.bad_best.score.best = -1

        self.bad_day = Profile('')
        self.bad_day.score.best = 1
        self.bad_day.score.day = 0

        self.bad_serie = Profile('')
        self.bad_serie.score.best = 1
        self.bad_serie.score.day = 1
        self.bad_serie.score.serie = -1

        self.profile = Profile('')
        self.profile.score.reset()

    def test_excercises_wrong_best(self):
        self.assertRaises(ValueError, self.exercise.get_training, self.bad_best)

    def test_exercises_wrong_day(self):
        self.assertRaises(ValueError, self.exercise.get_training, self.bad_day)
        self.bad_day.score.day = self.exercise.get_interval(0).last_day().no + 1
        self.assertRaises(ValueError, self.exercise.get_training, self.bad_day)

    def test_wrong_serie(self):
        self.assertRaises(ValueError, self.exercise.get_training, self.bad_serie)
        self.bad_serie.score.serie = self.exercise.get_interval(0).get_day(1).last_serie()[0] + 1
        self.assertRaises(ValueError, self.exercise.get_training, self.bad_serie)

    def test_get_training_bad(self):
        results = []
        results.append((Profile(''), None))
        results.append((Profile('', Score(14, 1, 1, 0)), Day(1, {1: 8, 2: 9, 3: 7, 4: 7, 5: 8})))
        results.append((Profile('', Score(25, 2, 1, 0)), Day(1, {1: 12, 2: 17, 3: 13, 4: 13, 5: 17})))
        results.append((Profile('', Score(1, 3, 2, 0)), Day(1, {1: 2, 2: 3, 3: 2, 4: 2, 5: 3})))
        results.append((Profile('', Score(88, 3, 3, 0)), Day(1, {1: 30, 2: 44, 3: 40, 4: 40, 5: 55})))
        results.append((Profile('', Score(99, 3, 1, 1)), Day(1, {1: 30, 2: 44, 3: 40, 4: 40, 5: 55})))
        results.append((Profile('', Score(56, 3, 5, 20)), Day(1, {1: 30, 2: 44, 3: 40, 4: 40, 5: 55})))

        for result in results:
            training = self.exercise.get_training(result[0])
            self.assertEquals(training, result[1], msg='%s' % result[0].score)

    def test_get_training_good(self):
        results = []
        results.append((Profile('', Score(12, 2, 0, 0)), Day(2, {1: 9, 2: 10, 3: 8, 4: 8, 5: 10})))
        results.append((Profile('', Score(1, 3, 5, 5)), Day(4, {1: 5, 2: 6, 3: 4, 4: 4, 5: 6})))
        results.append((Profile('', Score(1, 5, 5, 10)), Day(6, {1: 5, 2: 7, 3: 5, 4: 5, 5: 7})))
        results.append((Profile('', Score(1, 6, 6, 11)), None))

        for result in results:
            training = self.exercise.get_training(result[0])
            self.assertEquals(training, result[1], msg='%s' % result[0].score)


if __name__ == '__main__':
    unittest.main()
