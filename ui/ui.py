from tkinter import *
from tkinter import messagebox
from tkinter import ttk

import config
import properties
from core.model import Profile
from ui.frames import ProfilesFrame, ExerciseTypesFrame, ExcerciseFrame, ScheduleFrame, AddTrainnigFrame

try:
    import ui.dialogs as dialogs
except ImportError:
    import dialogs as dialogs

from logger import logging


class Muskul(Tk):
    def __init__(self):
        Tk.__init__(self)
        self.wm_title(properties.labels.TITLE)

        self.profile_info_panel = ProfileInfoPanel(self)
        self.profile_info_panel.pack(fill=X)

        self.container = ContainerPanel(self)
        self.container.pack(side="top", fill=BOTH, expand=True)

        self.bottom_panel = BottomPanel(self)
        self.bottom_panel.pack(fill=X, padx=3, pady=3)

        self.show_frame(properties.PROFILES)

    def refresh_frames(self):
        self.container.refresh_frames()

    def show_frame(self, window_name):
        self.container.show_frame(window_name)


class ContainerPanel(ttk.Frame):
    def __init__(self, parent):
        super().__init__(parent)

        self.parent = parent
        self.frames = []

        self.frames.append(ProfilesFrame(self, properties.PROFILES))

        frame = ExerciseTypesFrame(self, properties.EXERCISES_TYPE)
        frame.last_frame = properties.PROFILES
        self.frames.append(frame)

        frame = ExcerciseFrame(self, properties.PUSHUPS)
        frame.last_frame = properties.EXERCISES_TYPE
        self.frames.append(frame)

        frame = ScheduleFrame(self, properties.SCHEDULE)
        frame.last_frame = properties.PUSHUPS
        self.frames.append(frame)
        self.add_dynamic_frames()

    def add_dynamic_frames(self):
        frame = AddTrainnigFrame(self, properties.ADD_TRAINING)
        frame.last_frame = properties.PUSHUPS
        self.frames.append(frame)

        for frame in self.frames:
            frame.grid(row=0, column=0, sticky="nsew")

    def destroy_dynamic_frames(self):
        dynamic_frames = [properties.ADD_TRAINING]
        frames_copy = self.frames.copy()
        for dynamic_frame_name in dynamic_frames:
            for frame in frames_copy:
                if frame.name == dynamic_frame_name:
                    frame.destroy()
                    self.frames.remove(frame)
                    break

    def refresh_frames(self):
        self.destroy_dynamic_frames()
        self.add_dynamic_frames()

    def get_frame(self, name):
        for frame in self.frames:
            if frame.name == name:
                return frame
        raise AttributeError('No such frame %s' % name)

    def show_frame(self, window_name):
        frame = self.get_frame(window_name)
        frame.tkraise()
        self.parent.bottom_panel.back_button_available()

    def set_profile(self, profile):
        self.profile = profile
        self.parent.profile_info_panel.profile_name.set(profile.name)

    def get_profile(self):
        if self.profile:
            return self.profile
        return None

    def login(self):
        self.parent.profile_info_panel.login()


class ProfileInfoPanel(ttk.Frame):
    def __init__(self, parent_frame):
        super().__init__(parent_frame, height=30)
        self.parent_frame = parent_frame

        def logout(event):
            if event.type == '7':
                self.logout_label['foreground'] = 'red'
            elif event.type == '8':
                self.logout_label['foreground'] = 'black'
            elif event.type == '4':
                logging.info(self, 'logout', 'logout and restart traning')
                self.profile_name.set('')
                self.parent_frame.container.profile.score.restart()
                self.parent_frame.container.profile = None
                self.logout_label.pack_forget()
                self.profile_name_label.pack_forget()
                self.parent_frame.refresh_frames()
                self.parent_frame.show_frame(properties.PROFILES)

        self.logout_label = ttk.Label(self, text='logout')
        self.logout_label.bind('<Enter>', logout)
        self.logout_label.bind('<Leave>', logout)
        self.logout_label.bind('<Button-1>', logout)

        self.profile_name = StringVar()
        self.profile_name_label = ttk.Label(self, text='test', textvariable=self.profile_name)

    def login(self):
        self.logout_label.pack(side=RIGHT)
        self.profile_name_label.pack(side=RIGHT)


class BottomPanel(ttk.Frame):
    def __init__(self, parent_frame):
        ttk.Frame.__init__(self, parent_frame, borderwidth=config.FRAME_BORDER_WIDTH, relief=config.FRAME_BORDER_TYPE)
        self.parent_frame = parent_frame
        exit_btn = ttk.Button(self, text=properties.button.EXIT, command=self.exit)
        exit_btn.pack(side=RIGHT, pady=2)

        self.back_btn = ttk.Button(self, text=properties.button.BACK, command=self.back)
        self.back_btn.pack(side=RIGHT, pady=2)

    def exit(self):
        if messagebox.askyesno(message=properties.message.EXIT_MESSAGE, icon='question', title='Exit'):
            self.parent_frame.destroy()

    def back(self):
        frame = self.parent_frame.container.winfo_children()[-1]
        frame.back()

    def back_button_available(self):
        if self.parent_frame.container.winfo_children()[-1].last_frame:
            self.back_btn['state'] = 'enable'
        else:
            self.back_btn['state'] = 'disable'


if __name__ == '__main__':
    muskul = Muskul()
    muskul.show_frame(properties.ADD_TRAINING)
    profile = Profile('ala ma aids')
    profile.score.best = 56
    muskul.container.profile = profile
    trainingFrame = muskul.container.get_frame(properties.ADD_TRAINING)
    trainingFrame.setup()
    muskul.mainloop()
