__author__ = 'Paweł Dworniczak'

from tkinter import ttk
from tkinter import *

import properties


class InputTextDialog(Toplevel):
    def __init__(self, parent, method):
        Toplevel.__init__(self, parent)

        self.method = method
        self.message = ttk.Label(self, text=properties.message.ENTER_PROFILE_NAME)
        self.entry_name = ttk.Entry(self)
        self.entry_name.focus()
        self.ok_button = ttk.Button(self, text=properties.button.OK, command=self.ok)
        self.cancel_button = ttk.Button(self, text=properties.button.CANCEL, command=self.cancel)

        self.message.grid(column=0, row=0, columnspan=2)
        self.entry_name.grid(column=0, row=1, columnspan=2)
        self.ok_button.grid(column=0, row=2)
        self.cancel_button.grid(column=1, row=2)

    def cancel(self):
        self.destroy()

    def ok(self):
        self.method(self.entry_name.get())
        self.destroy()


if __name__ == '__main__':
    root = Tk()
    entry = InputTextDialog(root)
    root.mainloop()
    print(entry)
