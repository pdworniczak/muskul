from tkinter import StringVar, IntVar, ttk, N, S, E, W, Listbox, VERTICAL, messagebox, Text, DISABLED

import config
import core.model
import core.services
import properties
from core import excercises as exercises
from core.model import Profile
from logger import logging
from ui import dialogs as dialogs

__author__ = 'padw'


class AbstractFrame(ttk.Frame):
    def __init__(self, parent_frame, name=None):
        ttk.Frame.__init__(self, parent_frame, borderwidth=config.FRAME_BORDER_WIDTH, relief=config.FRAME_BORDER_TYPE)

        self.profiles = core.services.Profiles()
        self.parent_frame = parent_frame
        self.name = name
        self.last_frame = None

    def get_profile(self):
        return self.parent_frame.get_profile()

    def back(self):
        self.show_frame(self.last_frame)

    def login(self):
        self.parent_frame.login()

    def show_frame(self, window_name):
        self.parent_frame.show_frame(window_name)


class ProfilesFrame(AbstractFrame):
    def __init__(self, parent_frame, name):
        super().__init__(parent_frame, name)

        self.profile_names = StringVar()
        self.profile_names.set(self.profiles.get_profiles_names())

        profiles_frame = ttk.Labelframe(self, text=properties.labels.PROFILES)
        profiles_frame.pack(anchor='c')

        buttons_frame = ttk.Frame(profiles_frame)
        buttons_frame.grid(column=0, row=0, sticky=(N, S))

        create_profile_btn = ttk.Button(buttons_frame, text=properties.button.CREATE, command=self.add_profile)
        create_profile_btn.grid(column=0, row=0, padx=6, pady=2)

        self.delete_profile_btn = ttk.Button(buttons_frame, text=properties.button.DELETE, command=self.delete_profile)
        self.delete_profile_btn.grid(column=0, row=1, pady=2)
        self.delete_profile_btn['state'] = 'disabled'

        self.select_profile_btn = ttk.Button(buttons_frame, text=properties.button.SELECT, command=self.select_profile)
        self.select_profile_btn.grid(column=0, row=2, padx=6, pady=2, sticky=S)
        self.select_profile_btn['state'] = 'disabled'

        self.profiles_listbox = Listbox(profiles_frame, listvariable=self.profile_names)
        self.profiles_listbox.grid(column=1, row=0, sticky=(N, S, E, W))

        profiles_scrollbar = ttk.Scrollbar(profiles_frame, orient=VERTICAL, command=self.profiles_listbox.yview)
        profiles_scrollbar.grid(column=2, row=0, sticky=(N, S))

        self.profiles_listbox.bind('<<ListboxSelect>>', self.listbox_select_action)
        self.profiles_listbox['yscrollcommand'] = profiles_scrollbar.set

    def add_profile(self):
        def add_p(name):
            if name in self.profiles.get_profiles_names():
                messagebox.showinfo(properties.title.INFO, properties.message.PROFILE_EXIST % name)
                self.grab_release()
                return
            self.profiles.add_profile(Profile(name))
            self.profile_names.set(self.profiles.get_profiles_names())
            self.grab_release()

        add_dialog = dialogs.InputTextDialog(self, add_p)
        add_dialog.grab_set()

    def delete_profile(self):
        self.profiles.del_profile(self.profiles.get_profiles()[int(self.profiles_listbox.curselection()[0])])
        self.profile_names.set(self.profiles.get_profiles_names())
        self.profiles_listbox.event_generate('<<ListboxSelect>>')

    def select_profile(self):
        self.parent_frame.set_profile(self.profiles.get_profiles()[int(self.profiles_listbox.curselection()[0])])
        self.show_frame(properties.EXERCISES_TYPE)
        self.parent_frame.login()

    def listbox_select_action(self, event):
        if self.profiles_listbox.curselection():
            self.delete_profile_btn['state'] = 'enable'
            self.select_profile_btn['state'] = 'enable'
        else:
            self.delete_profile_btn['state'] = 'disable'
            self.select_profile_btn['state'] = 'disable'


class ExerciseTypesFrame(AbstractFrame):
    def __init__(self, parent_frame, name):
        super().__init__(parent_frame, name)
        self.excercicse_buttons = []
        for excercise in properties.EXERCISES_TYPES:
            button = ttk.Button(self, text=properties.PUSHUPS, command=lambda: self.select_excercise(excercise))
            button.pack()

    def select_excercise(self, exercises_name):
        logging.info(self, 'select_excercise', self.get_profile().name)
        self.show_frame(exercises_name)


class ExcerciseFrame(AbstractFrame):
    def __init__(self, parent_frame, name):
        super().__init__(parent_frame, name)

        button = ttk.Button(self, text=properties.labels.INFO, command=None)
        button.pack()

        button = ttk.Button(self, text=properties.labels.SCHEDULE, command=self.schedule)
        button.pack()

        button = ttk.Button(self, text=properties.labels.ADD_TRAINING, command=self.add_training)
        button.pack()

        button = ttk.Button(self, text=properties.labels.CALENDAR, command=None)
        button.pack()

    def schedule(self):
        logging.info(self, 'schedule', self.get_profile().name)
        self.show_frame(properties.SCHEDULE)

    def add_training(self):
        logging.info(self, 'add_training', self.get_profile().name)
        frame = self.parent_frame.get_frame(properties.ADD_TRAINING)
        frame.setup()
        self.show_frame(properties.ADD_TRAINING)


class ScheduleFrame(AbstractFrame):
    def __init__(self, parent_frame, name):
        super().__init__(parent_frame, name)

        self.text_field = Text(self, borderwidth=3, relief="sunken")
        self.text_field.insert('1.0', exercises.exercises[properties.PUSHUPS].schedule_view())
        self.text_field.config(state=DISABLED)
        self.text_field.grid(row=0, column=0, sticky="nsew", padx=2, pady=2)

        scrollb = ttk.Scrollbar(self, command=self.text_field.yview)
        scrollb.grid(row=0, column=1, sticky='nsew')
        self.text_field['yscrollcommand'] = scrollb.set


class AddTrainnigFrame(AbstractFrame):
    class Record(AbstractFrame):

        def __init__(self, parent_frame):
            super().__init__(parent_frame)

            label = ttk.Label(self, text="Score")
            label.pack()

            self.entry = ttk.Entry(self)
            self.entry.pack()

            button = ttk.Button(self, text="Add", command=self.set)
            button.pack()

        def set(self):
            profile = self.get_profile()
            profile.score.set_best(int(self.entry.get()))
            self.parent_frame.back_to_traning()
            logging.info(self, 'set', profile.name)

    class Normal(AbstractFrame):

        class SerieFrame(ttk.Frame):

            def __init__(self, parent_frame, profile, serie, value):
                super().__init__(parent_frame)

                self.parent_frame = parent_frame
                self.serie = serie
                self.value = value

                scope = exercises.exercises[properties.PUSHUPS].get_scope(profile)

                scope_label = ttk.Label(self, text='Scope: %s-%s' % scope)
                scope_label.grid(row=0, column=0, columnspan=2)

                scope_label = ttk.Label(self, text='Day: %s' % profile.score.day)
                scope_label.grid(row=1, column=0, columnspan=2)

                serie_label = ttk.Label(self, text='Serie: %s' % serie)
                serie_label.grid(row=2, column=0, columnspan=2)

                self.result = IntVar()
                self.result.set(value)

                self.result_label = ttk.Label(self, textvariable=self.result)
                self.result_label.grid(row=3, column=0, rowspan=2)

                up_button = ttk.Button(self, text='up', command=self.up)
                up_button.grid(row=3, column=1)

                down_button = ttk.Button(self, text='down', command=self.down)
                down_button.grid(row=4, column=1)

                done_button = ttk.Button(self, text='done', command=self.done)
                done_button.grid(row=5, column=0, columnspan=2)

            def up(self):
                result = self.result.get()
                result += 1
                self.result.set(result)
                self.result_color(result)

            def down(self):
                result = self.result.get()
                result -= 1
                self.result.set(result)
                self.result_color(result)

            def result_color(self, result):
                if result < self.value:
                    self.result_label['foreground'] = 'red'
                elif result > self.value:
                    self.result_label['foreground'] = 'green'
                else:
                    self.result_label['foreground'] = 'black'

            def done(self):
                self.destroy()
                self.parent_frame.serie_frames.remove(self)
                self.parent_frame.result(self.serie, self.result.get(), self.value)

        def __init__(self, parent_frame, training_day):
            super().__init__(parent_frame)

            serie = training_day.get_series()
            logging.info(self, '__init__', str(serie))

            self.serie_frames = []

            self.profile = self.get_profile()
            for k, v in serie.items():
                frame = AddTrainnigFrame.Normal.SerieFrame(self, self.profile, k, v)
                self.serie_frames.append(frame)

            for frame in list(reversed(self.serie_frames)):
                frame.grid(row=0, column=0, sticky="nsew")
                frame.tkraise()

        def result(self, serie, result, expected_result):
            self.profile.score.set_score(serie, result)
            if result < expected_result:
                logging.info(self, 'result', 'fail')
                for frame in self.serie_frames:
                    frame.destroy()
                ttk.Button(self, text='FAIL', command=self.parent_frame.back_to_traning).pack()
            elif self.serie_frames:
                logging.info(self, 'result', 'succes, next')
            else:
                logging.info(self, 'result', 'succes')
                ttk.Button(self, text='GOOD', command=self.parent_frame.back_to_traning).pack()

    def __init__(self, parent_frame, name):
        super().__init__(parent_frame, name)

        self.title_label = ttk.Label(self, text=properties.labels.ADD_TRAINING)
        self.title_label.pack()

    def setup(self):
        logging.info(self, 'setup', self.get_profile().name)
        training_day = exercises.exercises[properties.PUSHUPS].get_training(self.get_profile())
        if training_day:
            self.add_frame = AddTrainnigFrame.Normal(self, training_day)
        else:
            self.add_frame = AddTrainnigFrame.Record(self)
        self.add_frame.pack()

    def back(self):
        logging.info(self, 'back', 'back and restart traning')
        self.get_profile().score.restart()
        self.back_to_traning()

    def back_to_traning(self):
        logging.info(self, 'back_to_traning', 'back to traning')
        self.add_frame.destroy()
        super().back()
